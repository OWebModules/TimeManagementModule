﻿using OntologyAppDBConnector;
using OntologyAppDBConnector.Models;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeManagementModule.Models;
using TimeManagementModule.Services;

namespace TimeManagementModule
{
    public class TimeManagementController : AppController
    {
        private BaseConfig baseConfig;
        public BaseConfig BaseConfig
        {
            get
            {
                return baseConfig;
            }
        }

        public clsOntologyItem ClassTimeManagement
        {
            get
            {
                return Config.LocalData.Class_Timemanagement;
            }
        }

        public clsOntologyItem StateWork
        {
            get
            {
                return Config.LocalData.Object_Work;
            }
        }
        public clsOntologyItem StatePrivate
        {
            get
            {
                return Config.LocalData.Object_private;
            }
        }

        public clsOntologyItem StateUrlaub
        {
            get
            {
                return Config.LocalData.Object_Urlaub;
            }
        }

        public clsOntologyItem StateKrank
        {
            get
            {
                return Config.LocalData.Object_Krank;
            }
        }


        public async Task<ResultOItem<ClassObject>> GetOItem(string idItem)
        {
            var serviceElastic = new ServiceAgentElastic(Globals);
            return await serviceElastic.GetClassObject(idItem);
        }

        public async Task<ResultItem<StopWatchMeasureResult>> GetStopWatchMeasure(StopWatchMeasureRequest request)
        {
            var taskResult = await Task.Run<ResultItem<StopWatchMeasureResult>>(async () =>
            {
                var result = new ResultItem<StopWatchMeasureResult>
                {
                    ResultState = Globals.LState_Success.Clone()
                };

                var timeSatmp = DateTime.Now;

                if (string.IsNullOrEmpty(request.IdStopWatch))
                {
                    request.IdStopWatch = Globals.NewGUID;
                }

                if (!Globals.is_GUID(request.IdStopWatch))
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "StopWatchId is no valid GUID! You have to provide a valid StopWatchId!";
                    return result;
                }

                if (string.IsNullOrEmpty(request.IdUser))
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "UserId is empty! You have to provide a valid UserId!";
                    return result;
                }

                if (!Globals.is_GUID(request.IdUser))
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "UserId is no valid GUID! You have to provide a valid UserId!";
                    return result;
                }

                if (string.IsNullOrEmpty(request.IdGroup))
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "GroupId is empty! You have to provide a valid GroupId!";
                    return result;
                }

                if (!Globals.is_GUID(request.IdGroup))
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "GroupId is no valid GUID! You have to provide a valid GroupId!";
                    return result;
                }

                var serviceAgent = new ServiceAgentElastic(Globals);
                if (baseConfig == null)
                {
                    var baseConfigResult = await serviceAgent.GetBaseConfig(request.IdUser, request.IdGroup);
                    if (baseConfigResult.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState = baseConfigResult.ResultState;
                        return result;
                    }

                    baseConfig = new BaseConfig(baseConfigResult.Result);
                }

                var elasticSearchConfig = new ElasticSearchConfigModule.ElasticSearchConfigController(Globals);

                var resultGetConfig = await elasticSearchConfig.GetConfig(baseConfig.BaseConfigRaw.StopWatchConfig.GUID,
                     StopWatch.Config.LocalData.RelationType_belonging,
                     StopWatch.Config.LocalData.RelationType_belonging,
                     Globals.Direction_LeftRight);

                result.ResultState = resultGetConfig.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                if (resultGetConfig.Result == null)
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No Index or Type found!";
                    return result;
                }

                var dbReader = new ElasticSearchNestConnector.clsUserAppDBSelector(resultGetConfig.Result.NameServer,
                    resultGetConfig.Result.Port,
                    resultGetConfig.Result.NameIndex, Globals.SearchRange, Globals.Session);
                var dbWriter = new ElasticSearchNestConnector.clsUserAppDBUpdater(dbReader);

                var stopWatchEntriesResult = dbReader.GetData_Documents(500, resultGetConfig.Result.NameIndex, resultGetConfig.Result.NameType, $"{nameof(StopWatchEntry.StopWatchId)}:{request.IdStopWatch}");
                if (!stopWatchEntriesResult.IsOK)
                {
                    result.ResultState = Globals.LState_Error.Clone();
                    result.ResultState.Additional1 = stopWatchEntriesResult.ErrorMessage;
                }

                result.Result =new StopWatchMeasureResult( stopWatchEntriesResult.Documents.Select(doc => new StopWatchEntry(doc.Dict)).ToList());

                return result;
            });
            return taskResult;
        }
        public async Task<ResultItem<List<StopWatchEntry>>> TriggerStopWatch(StopWatchRequest request)
        {
            var taskResult = await Task.Run<ResultItem<List<StopWatchEntry>>>(async() =>
           {
               var result = new ResultItem<List<StopWatchEntry>>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new List<StopWatchEntry>()
               };

               var timeSatmp = DateTime.Now;

               if (string.IsNullOrEmpty(request.IdStopWatch))
               {
                   request.IdStopWatch = Globals.NewGUID;
               }

               if (!Globals.is_GUID(request.IdStopWatch))
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "StopWatchId is no valid GUID! You have to provide a valid StopWatchId!";
                   return result;
               }

               if (string.IsNullOrEmpty(request.IdUser))
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "UserId is empty! You have to provide a valid UserId!";
                   return result;
               }

               if (!Globals.is_GUID(request.IdUser))
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "UserId is no valid GUID! You have to provide a valid UserId!";
                   return result;
               }

               if (string.IsNullOrEmpty(request.IdGroup))
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "GroupId is empty! You have to provide a valid GroupId!";
                   return result;
               }

               if (!Globals.is_GUID(request.IdGroup))
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "GroupId is no valid GUID! You have to provide a valid GroupId!";
                   return result;
               }

               var serviceAgent = new ServiceAgentElastic(Globals);
               if (baseConfig == null)
               {
                   var baseConfigResult = await serviceAgent.GetBaseConfig(request.IdUser, request.IdGroup);
                   if (baseConfigResult.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       result.ResultState = baseConfigResult.ResultState;
                       return result;
                   }

                   baseConfig = new BaseConfig(baseConfigResult.Result);
               }

               var elasticSearchConfig = new ElasticSearchConfigModule.ElasticSearchConfigController(Globals);

               var resultGetConfig = await elasticSearchConfig.GetConfig(baseConfig.BaseConfigRaw.StopWatchConfig.GUID, 
                    StopWatch.Config.LocalData.RelationType_belonging, 
                    StopWatch.Config.LocalData.RelationType_belonging,
                    Globals.Direction_LeftRight);

               result.ResultState = resultGetConfig.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   return result;
               }

               if (resultGetConfig.Result == null)
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "No Index or Type found!";
                   return result;
               }

               var dbReader = new ElasticSearchNestConnector.clsUserAppDBSelector(resultGetConfig.Result.NameServer,
                   resultGetConfig.Result.Port,
                   resultGetConfig.Result.NameIndex, Globals.SearchRange, Globals.Session);
               var dbWriter = new ElasticSearchNestConnector.clsUserAppDBUpdater(dbReader);

               var stopWatchEntriesResult = dbReader.GetData_Documents(500, resultGetConfig.Result.NameIndex, resultGetConfig.Result.NameType, $"{nameof(StopWatchEntry.StopWatchId)}:{request.IdStopWatch}");
               if (!stopWatchEntriesResult.IsOK )
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = stopWatchEntriesResult.ErrorMessage;
               }

               result.Result = stopWatchEntriesResult.Documents.Select(doc => new StopWatchEntry(doc.Dict)).ToList();

               var newEntry = new StopWatchEntry(request.IdStopWatch, (int)request.Mode, timeSatmp)
               {
                   OrderId = result.Result.Count + 1
               };

               var saveResult = dbWriter.SaveDoc<StopWatchEntry>(new List<StopWatchEntry> { newEntry }, nameof(StopWatchEntry.Id), resultGetConfig.Result.NameType);

               if (saveResult.GUID == Globals.LState_Error.GUID)
               {
                   result.ResultState = Globals.LState_Error.Clone();
                   result.ResultState.Additional1 = "Error while saving the Stop-Watch entry!";
                   return result;
               }

               result.Result.Add(newEntry);

               return result;
           });
            return taskResult;
        }

        public async Task<ResultItem<TimeManagementEntryList>> GetTimeManagementEntryList( clsOntologyItem userItem, 
            clsOntologyItem groupItem, 
            clsOntologyItem refItem = null, 
            TimeManagementEntryListFilter filterItem = null)
        {
            var serviceAgent = new ServiceAgentElastic(Globals);

            var result = new ResultItem<TimeManagementEntryList>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new Models.TimeManagementEntryList
                {
                    EntryList = new List<TimeManagementEntry>()
                }
            };


            if (baseConfig == null)
            {
                var baseConfigResult = await serviceAgent.GetBaseConfig(userItem, groupItem);
                if (baseConfigResult.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    result.ResultState = baseConfigResult.ResultState;

                    return result;
                }

                baseConfig = new BaseConfig(baseConfigResult.Result);
            }
            var serviceResult = await serviceAgent.GetTimeManagementEntries(baseConfig, refItem, filterItem: filterItem);

            if (serviceResult.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = serviceResult.ResultState;

                return result;
            }

            var factory = new Factories.TimeManagementEntryFactory(Globals);
            var factoryResult = await factory.GetTimeManagementItemList(serviceResult.Result, baseConfig, userItem, groupItem);

            if (factoryResult.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result.ResultState = factoryResult.ResultState;

                return result;
            }

            var lastEnd = DateTime.Now;
            if (factoryResult.Result.Any())
            {
                lastEnd = factoryResult.Result.Max(entry => entry.Ende);
            }


            result.Result.EntryList = factoryResult.Result;
            result.Result.LastEnd = lastEnd;

            return result;
        }

        public async Task<ResultItem<TimeManagementEntry>> GetTimeManagementEntry(GetTimeManagementEntryRequest request)
        {
            var taskResult = await Task.Run(async() =>
            {
                var result = new ResultItem<TimeManagementEntry>
                {
                    ResultState = Globals.LState_Success.Clone()
                };

                var serviceElastic = new ServiceAgentElastic(Globals);
                clsOntologyItem reference = null;
                clsOntologyItem referenceParent = null;
                clsOntologyItem timeManagementItem = null;

                if (baseConfig == null)
                {
                    var baseConfigResult = await serviceElastic.GetBaseConfig(request.UserItem, request.GroupItem);
                    if (baseConfigResult.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        result.ResultState = baseConfigResult.ResultState;

                        return result;
                    }

                    baseConfig = new BaseConfig(baseConfigResult.Result);
                }

                if (!string.IsNullOrEmpty(request.IdReference))
                {
                    var referenceResult = await serviceElastic.GetOItem(request.IdReference);
                    result.ResultState = referenceResult.ResultState;
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                    reference = referenceResult.Result;
                    if (reference.GUID_Parent == Config.LocalData.Class_Timemanagement.GUID)
                    {
                        timeManagementItem = reference;
                        reference = null;
                    }
                    else if (!string.IsNullOrEmpty(reference.GUID_Parent) && reference.Type == Globals.Type_Object)
                    {
                        var referenceParentResult = await serviceElastic.GetOItem(reference.GUID_Parent);
                        result.ResultState = referenceParentResult.ResultState;
                        if (result.ResultState.GUID == Globals.LState_Error.GUID)
                        {
                            request.MessageOutput?.OutputError(result.ResultState.Additional1);
                            return result;
                        }
                        referenceParent = referenceParentResult.Result;
                    }
                    
                }
                
                if (!string.IsNullOrEmpty(request.IdTimeManagementEntry) && timeManagementItem == null)
                {
                    var timeManagementResult = await serviceElastic.GetOItem(request.IdTimeManagementEntry);
                    result.ResultState = timeManagementResult.ResultState;
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                    timeManagementItem = timeManagementResult.Result;
                }

                if (timeManagementItem != null)
                {
                    var itemsRawResult = await serviceElastic.GetTimeManagementEntries(baseConfig, null, timeManagementItem.GUID);
                    result.ResultState = itemsRawResult.ResultState;
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                    var factory = new Factories.TimeManagementEntryFactory(Globals);
                    var factoryResult = await factory.GetTimeManagementItemList(itemsRawResult.Result, baseConfig, request.UserItem, request.GroupItem);
                    result.ResultState = factoryResult.ResultState;
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                    result.Result = factoryResult.Result.FirstOrDefault();
                    result.Result.SetUndoItem();
                }
                else 
                {
                    result.Result = new TimeManagementEntry(null, null, reference?.GUID, reference?.Name, referenceParent?.GUID, referenceParent?.Name, Config.LocalData.Object_Work.GUID, Config.LocalData.Object_Work.Name, null, DateTime.Now, null, DateTime.Now, request.GroupItem.GUID, request.GroupItem.Name, request.UserItem.GUID, request.UserItem.Name);
                }

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SaveRefRel(TimeManagementEntry entry, clsOntologyItem refItem)
        {
            var result = Globals.LState_Success.Clone();

            var elasticAgent = new Services.ServiceAgentElastic(Globals);

            var oTimeManagementEntry = new clsOntologyItem
            {
                GUID = entry.IdTimeManagement,
                Name = entry.NameTimeManagement,
                GUID_Parent = Config.LocalData.Class_Timemanagement.GUID,
                Type = Globals.Type_Object
            };

            result = await elasticAgent.SaveRefRel(oTimeManagementEntry, refItem);

            return result;
        }


        public async Task<clsOntologyItem> SaveUserGroup(TimeManagementEntry entry, clsOntologyItem userItem, clsOntologyItem groupItem)
        {
            var result = Globals.LState_Success.Clone();

            var elasticAgent = new Services.ServiceAgentElastic(Globals);

            var oTimeManagementEntry = new clsOntologyItem
            {
                GUID = entry.IdTimeManagement,
                Name = entry.NameTimeManagement,
                GUID_Parent = Config.LocalData.Class_Timemanagement.GUID,
                Type = Globals.Type_Object
            };

            result = await elasticAgent.SaveUserGroup(oTimeManagementEntry, userItem, groupItem);

            return result;
        }

        public async Task<ResultItem<TimeManagementEntry>> SaveEntry(TimeManagementEntry entry)
        {
            var result = new ResultItem<TimeManagementEntry>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = entry
            };

            var elasticAgent = new Services.ServiceAgentElastic(Globals);
            var isNew = false;
            if (string.IsNullOrEmpty(entry.IdTimeManagement))
            {
                isNew = true;
            }

            if (isNew || entry.GetUndoItem().NameTimeManagement != entry.NameTimeManagement)
            {
                var serviceResult = await elasticAgent.SaveTimeManagementEntry(entry, nameof(entry.NameTimeManagement));

                result.ResultState = serviceResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                
            }

            if (isNew || entry.GetUndoItem().Start != entry.Start)
            {
                var serviceResult = await elasticAgent.SaveTimeManagementEntry(entry, nameof(entry.Start));

                result.ResultState = serviceResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }
            }

            if (isNew || entry.GetUndoItem().Start != entry.Ende)
            {
                var serviceResult = await elasticAgent.SaveTimeManagementEntry(entry, nameof(entry.Ende));

                result.ResultState = serviceResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }
            }

            if (isNew  || entry.GetUndoItem().IdLogState != entry.IdLogState)
            {
                if (!string.IsNullOrEmpty(entry.IdLogState))
                {
                    var serviceResult = await elasticAgent.SaveTimeManagementEntry(entry, nameof(entry.IdLogState));

                    result.ResultState = serviceResult.ResultState;

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }

            }

            if (isNew || entry.GetUndoItem().IdReference != entry.IdReference)
            {
                if (!string.IsNullOrEmpty(entry.IdReference))
                {
                    var serviceResult = await elasticAgent.SaveTimeManagementEntry(entry, nameof(entry.IdReference));

                    result.ResultState = serviceResult.ResultState;

                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        return result;
                    }
                } else if (!isNew)
                {
                    result.ResultState = await elasticAgent.RemoveRefRel(entry);
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }
            }

            if (isNew || entry.GetUndoItem().IdUser != entry.IdUser)
            {
                var serviceResult = await elasticAgent.SaveTimeManagementEntry(entry, nameof(entry.IdUser));

                result.ResultState = serviceResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }
            }

            if (isNew || entry.GetUndoItem().IdGroup != entry.IdGroup)
            {
                var serviceResult = await elasticAgent.SaveTimeManagementEntry(entry, nameof(entry.IdGroup));

                result.ResultState = serviceResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }
            }

            entry.SetUndoItem();

            return result;
        }

        public TimeManagementController(Globals globals) : base(globals)
        {   
        }

        public async Task<clsOntologyItem> SetBaseConfig(clsOntologyItem userItem, clsOntologyItem groupItem)
        {
            var result = Globals.LState_Success.Clone();
            var serviceAgent = new ServiceAgentElastic(Globals);
            var baseConfigResult = await serviceAgent.GetBaseConfig(userItem.GUID, groupItem.GUID);
            if (baseConfigResult.ResultState.GUID == Globals.LState_Error.GUID)
            {
                result = baseConfigResult.ResultState;
                return result;
            }

            baseConfig = new BaseConfig(baseConfigResult.Result);

            return result;
        }
    }
}
