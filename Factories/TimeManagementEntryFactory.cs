﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeManagementModule.Models;

namespace TimeManagementModule.Factories
{
    public class TimeManagementEntryFactory
    {
        private Globals globals;
        public async Task<ResultItem<List<TimeManagementEntry>>> GetTimeManagementItemList(TimeManagementEntriesRaw rawItems, BaseConfig baseConfig, clsOntologyItem userItem, clsOntologyItem groupItem)
        {
            var taskResult = await Task.Run<ResultItem<List<TimeManagementEntry>>>(() =>
            {
                var result = new ResultItem<List<TimeManagementEntry>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<TimeManagementEntry>()
                };

                var users = rawItems.Relations.Where(p => p.ID_Other == (userItem != null ? userItem.GUID : baseConfig.IdUser)).ToList();
                var groups = rawItems.Relations.Where(p => p.ID_Other == (groupItem != null ? groupItem.GUID : baseConfig.IdGroup)).ToList();
                result.Result = (from timeManagementEntry in rawItems.TimeManagementEntries
                                 join objStart in rawItems.Attributes.Where(p => p.ID_AttributeType == Config.LocalData.AttributeType_Start.GUID).ToList() on timeManagementEntry.GUID equals objStart.ID_Object
                                 join objEnde in rawItems.Attributes.Where(p => p.ID_AttributeType == Config.LocalData.AttributeType_Ende.GUID).ToList() on timeManagementEntry.GUID equals objEnde.ID_Object
                                 join objState in rawItems.Relations.Where(p => p.ID_Parent_Other == Config.LocalData.Class_Logstate.GUID).ToList() on timeManagementEntry.GUID equals objState.ID_Object into objState1
                                 from objState in objState1.DefaultIfEmpty()
                                 join objUser in users on timeManagementEntry.GUID equals objUser.ID_Object
                                 join objRelation in rawItems.TimeManagementEntriesToRef on timeManagementEntry.GUID equals objRelation.ID_Object into objRelations
                                 from objRelation in objRelations.DefaultIfEmpty()
                                 join objGroup in groups on timeManagementEntry.GUID equals objGroup.ID_Object
                                 select new
                                 {
                                     IdTimeManagement = timeManagementEntry.GUID,
                                     NameTimeManagement = timeManagementEntry.Name,
                                     IdLogState = objState?.ID_Other,
                                     NameLogState = objState?.Name_Other,
                                     IdAttributeStart = objStart?.ID_Attribute,
                                     Start = objStart.Val_Date != null ? (DateTime)objStart.Val_Date : DateTime.Now,
                                     IdAttributeEnde = objEnde?.ID_Attribute,
                                     Ende = objEnde.Val_Date != null ? (DateTime)objEnde.Val_Date : DateTime.Now,
                                     IdRel = objRelation?.ID_Other,
                                     NameRel = objRelation?.Name_Other,
                                     IdParRel = objRelation?.ID_Parent_Other,
                                     NameParRel = objRelation?.Name_Parent_Other,
                                     IdGroup = objGroup.ID_Other,
                                     NameGroup = objGroup.Name_Other,
                                     IdUser = objUser.ID_Other,
                                     NameUser = objUser.Name_Other
                                 }).Select(p =>
                                         new TimeManagementEntry(p.IdTimeManagement,
                                              p.NameTimeManagement,
                                             p.IdRel,
                                             p.NameRel,
                                             p.IdParRel,
                                             p.NameParRel,
                                             p.IdLogState,
                                             p.NameLogState,
                                             p.IdAttributeStart,
                                             p.Start,
                                             p.IdAttributeEnde,
                                             p.Ende, 
                                             p.IdGroup,
                                             p.NameGroup,
                                             p.IdUser,
                                             p.NameUser)).ToList();

                var weeks = (from objTimeManagement in result.Result
                             group objTimeManagement by new { objTimeManagement.YearStart, objTimeManagement.WeekStart } into objWeeks
                             select new
                             {
                                 Year = objWeeks.Key.YearStart,
                                 Week = objWeeks.Key.WeekStart,
                                 Duration_Hours_Week = objWeeks.Where(p => p.IdLogState != Config.LocalData.Object_private.GUID).Sum(p => p.Ende.Subtract(p.Start).TotalSeconds / 3600),
                                 Duration_Minutes_Week = objWeeks.Where(p => p.IdLogState != Config.LocalData.Object_private.GUID).Sum(p => p.Ende.Subtract(p.Start).TotalSeconds / 60)
                             }).ToList();

                var days = (from objTimeManagement in result.Result
                            group objTimeManagement by new { objTimeManagement.YearStart, objTimeManagement.MonthStart, objTimeManagement.DayStart } into objDays
                            select new
                            {
                                Year = objDays.Key.YearStart,
                                Month = objDays.Key.MonthStart,
                                Day = objDays.Key.DayStart,
                                Last_Ende = objDays.Max(p => p.Ende),
                                Duration_Hours_Day = objDays.Where(p => p.IdLogState != Config.LocalData.Object_private.GUID).Sum(p => p.Ende.Subtract(p.Start).TotalSeconds / 3600),
                                Duration_Minutes_Day = objDays.Where(p => p.IdLogState != Config.LocalData.Object_private.GUID).Sum(p => p.Ende.Subtract(p.Start).TotalSeconds / 60),
                                DaySeq = objDays.Key.YearStart * 10000 + objDays.Key.MonthStart * 100 + objDays.Key.DayStart
                            }).ToList();

                var lastEntries = (from objDay in days
                                   from objTimeManagement in
                                       result.Result.Where(
                                           t => t.StartSeq == objDay.DaySeq)
                                                              .OrderByDescending(t => t.Ende)
                                                              .Take(1)
                                   select objTimeManagement).ToList();

                result.Result = (from objTimeManagement in result.Result
                                 join objWeek in weeks on new { Year = objTimeManagement.YearStart, Week = objTimeManagement.WeekStart } equals
                                                                                    new { Year = objWeek.Year, Week = objWeek.Week }
                                 join objDay in days on new { Year = objTimeManagement.YearStart, Month = objTimeManagement.MonthStart, Day = objTimeManagement.DayStart } equals
                                                                 new { Year = objDay.Year, Month = objDay.Month, Day = objDay.Day }
                                 join objLastEntry in lastEntries on new { Year = objTimeManagement.YearStart, Month = objTimeManagement.MonthStart, Day = objTimeManagement.DayStart } equals
                                                                 new { Year = objLastEntry.YearStart, Month = objLastEntry.MonthStart, Day = objLastEntry.DayStart }
                                 select new TimeManagementEntry(objTimeManagement.IdTimeManagement,
                                       objTimeManagement.NameTimeManagement,
                                       objTimeManagement.IdReference,
                                       objTimeManagement.NameReference,
                                       objTimeManagement.IdParentReference,
                                       objTimeManagement.NameParentReference,
                                       objTimeManagement.IdLogState,
                                       objTimeManagement.NameLogState,
                                       objTimeManagement.IdAttributeStart,
                                       objTimeManagement.Start,
                                       objTimeManagement.IdAttributeEnde,
                                       objTimeManagement.Ende,
                                       baseConfig.IdGroup,
                                       baseConfig.NameGroup,
                                       baseConfig.IdUser,
                                       baseConfig.NameUser)
                                 {
                                     ToDoEnd = objLastEntry.ToDoEnd,
                                     DurationMinutesWeek = objWeek.Duration_Minutes_Week,
                                     DurationHoursWeek = objWeek.Duration_Hours_Week,
                                     DurationHoursDay = objDay.Duration_Hours_Day,
                                     DurationMinutesDay = objDay.Duration_Minutes_Day
                                 }).ToList();
                return result;
            });

            return taskResult;
        }

        public TimeManagementEntryFactory(Globals globals)
        {
            this.globals = globals;
        }
    }
}
