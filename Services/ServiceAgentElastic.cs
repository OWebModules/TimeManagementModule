﻿using ElasticSearchNestConnector;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Services;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeManagementModule.Models;

namespace TimeManagementModule.Services
{
    public class ServiceAgentElastic: ElasticBaseAgent
    {

        public async Task<ResultItem<TimeManagementBaseConfigRaw>> GetBaseConfig(string idUser, string idGroup)
        {
            var taskResult = await Task.Run<ResultItem<TimeManagementBaseConfigRaw>>(async() =>
            {
                var result = new ResultItem<TimeManagementBaseConfigRaw>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new TimeManagementBaseConfigRaw()
                };

                var dbReaderUserGroup = new OntologyModDBConnector(globals);

                var userItem = dbReaderUserGroup.GetOItem(idUser, globals.Type_Object);

                if (userItem.GUID_Related == globals.LState_Error.GUID)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "Error while getting the Useritem!";
                    return result;
                }

                var groupItem = dbReaderUserGroup.GetOItem(idGroup, globals.Type_Object);

                if (userItem.GUID_Related == globals.LState_Error.GUID)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "Error while getting the Groupitem!";
                    return result;
                }

                result = await GetBaseConfig(userItem, groupItem);

                return result;
            });

            return taskResult;

        }
        public async Task<ResultItem<TimeManagementBaseConfigRaw>> GetBaseConfig(clsOntologyItem user, clsOntologyItem group)
        {


            var taskResult = await Task.Run<ResultItem<TimeManagementBaseConfigRaw>>(() =>
            {
                var result = new ResultItem<TimeManagementBaseConfigRaw>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new TimeManagementBaseConfigRaw()
                };

                result.Result.User = user;
                result.Result.Group = group;

                var searchUserWorkConfigToUserGroup = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Parent_Other = result.Result.Group.GUID_Parent,
                        ID_Parent_Object = Config.LocalData.ClassRel_User_Work_Config_belongs_to_Group.ID_Class_Left,
                        ID_RelationType = Config.LocalData.ClassRel_User_Work_Config_belongs_to_Group.ID_RelationType
                    },
                    new clsObjectRel
                    {
                        ID_Parent_Other = result.Result.User.GUID_Parent,
                        ID_Parent_Object = Config.LocalData.ClassRel_User_Work_Config_belongs_to_user.ID_Class_Left,
                        ID_RelationType = Config.LocalData.ClassRel_User_Work_Config_belongs_to_user.ID_RelationType
                    }
                };

                var dbReaderUserGroupConfigToUserGroup = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderUserGroupConfigToUserGroup.GetDataObjectRel(searchUserWorkConfigToUserGroup);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the User-Group Config!";
                    return result;
                }

                result.Result.UserGroupConfig = (from objUser in dbReaderUserGroupConfigToUserGroup.ObjectRels.Where(uw => uw.ID_Other == result.Result.User.GUID).ToList()
                                             join objGroup in dbReaderUserGroupConfigToUserGroup.ObjectRels.Where(uw => uw.ID_Other == result.Result.Group.GUID).ToList() on objUser.ID_Object equals objGroup.ID_Object
                                             select objUser).FirstOrDefault();

                if (result.Result.UserGroupConfig == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No User-Group Found!";
                    return result;
                }

                var searchStandardHoursAndDayCounts = new List<clsObjectAtt>
                {
                    new clsObjectAtt
                    {
                        ID_Object = result.Result.UserGroupConfig.ID_Object,
                        ID_AttributeType = Config.LocalData.ClassAtt_User_Work_Config_Hours.ID_AttributeType
                    }
                };

                searchStandardHoursAndDayCounts.Add(new clsObjectAtt
                {
                    ID_Object = result.Result.UserGroupConfig.ID_Object,
                    ID_AttributeType = Config.LocalData.ClassAtt_User_Work_Config_Workdays.ID_AttributeType
                });

                var dbReaderStandardHoursAndDayCounts = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderStandardHoursAndDayCounts.GetDataObjectAtt(searchStandardHoursAndDayCounts);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Hours and Days count!";
                    return result;
                }

                var hours = dbReaderStandardHoursAndDayCounts.ObjAtts.Where(h => h.ID_AttributeType == Config.LocalData.ClassAtt_User_Work_Config_Hours.ID_AttributeType).ToList();
                var dayCounts = dbReaderStandardHoursAndDayCounts.ObjAtts.Where(h => h.ID_AttributeType == Config.LocalData.ClassAtt_User_Work_Config_Workdays.ID_AttributeType).ToList();

                if (!hours.Any() || !dayCounts.Any())
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No hours or no days found!";
                    return result;
                }

                result.Result.StandardHours = hours.First();
                result.Result.StandardDayCount = dayCounts.First();

                var searchStopWatchConfig = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Other = result.Result.UserGroupConfig.ID_Object,
                        ID_RelationType = StopWatch.Config.LocalData.ClassRel_StopWatch_belongs_to_User_Work_Config.ID_RelationType,
                        ID_Parent_Object = StopWatch.Config.LocalData.ClassRel_StopWatch_belongs_to_User_Work_Config.ID_Class_Left
                    }
                };

                var dbReaderStopWatchConfig = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderStopWatchConfig.GetDataObjectRel(searchStopWatchConfig);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Stop-Watch config!";
                    return result;
                }

                result.Result.StopWatchConfig = dbReaderStopWatchConfig.ObjectRels.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Object,
                    Name = rel.Name_Object,
                    GUID_Parent = rel.ID_Parent_Object,
                    Type = globals.Type_Object
                }).FirstOrDefault();

                if (result.Result.StopWatchConfig != null)
                {
                    var searchElasticSearchIndex = new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Object = result.Result.StopWatchConfig.GUID,
                            ID_RelationType = StopWatch.Config.LocalData.ClassRel_StopWatch_belonging_Indexes__Elastic_Search_.ID_RelationType,
                            ID_Parent_Other = StopWatch.Config.LocalData.ClassRel_StopWatch_belonging_Indexes__Elastic_Search_.ID_Class_Right
                        }
                    };

                    var dbReaderElasticSearchIndex = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderElasticSearchIndex.GetDataObjectRel(searchElasticSearchIndex);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the ElasticSearch Index of Stop-Watch config!";
                        return result;
                    }

                    result.Result.ElasticSearchIndex = dbReaderElasticSearchIndex.ObjectRels.Select(rel => new clsOntologyItem
                    {
                        GUID = rel.ID_Other,
                        Name = rel.Name_Other,
                        GUID_Parent = rel.ID_Parent_Other,
                        Type = rel.Ontology
                    }).FirstOrDefault();

                    if (result.Result.ElasticSearchIndex == null)
                    {
                        result.ResultState = globals.LState_Error.Clone();
                        result.ResultState.Additional1 = "You have to provide an ElasticSearch Index, if you define a Stop-Watch config!";
                        return result;
                    }

                    var searchESType = new List<clsObjectRel>
                    {
                        new clsObjectRel
                        {
                            ID_Object = result.Result.StopWatchConfig.GUID,
                            ID_RelationType = StopWatch.Config.LocalData.ClassRel_StopWatch_belonging_Types__Elastic_Search_.ID_RelationType,
                            ID_Parent_Other = StopWatch.Config.LocalData.ClassRel_StopWatch_belonging_Types__Elastic_Search_.ID_Class_Right
                        }
                    };

                    var dbReaderESType = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderESType.GetDataObjectRel(searchESType);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the ElasticSearch Type!";
                        return result;
                    }

                    result.Result.ElasticSearchType = dbReaderESType.ObjectRels.Select(rel => new clsOntologyItem
                    {
                        GUID = rel.ID_Other,
                        Name = rel.Name_Other,
                        GUID_Parent = rel.ID_Parent_Other,
                        Type = rel.Ontology
                    }).FirstOrDefault();

                    if (result.Result.ElasticSearchType == null)
                    {
                        result.ResultState = globals.LState_Error.Clone();
                        result.ResultState.Additional1 = "You have to provide an ElasticSearch Type, if you define a Stop-Watch config!";
                        return result;
                    }
                }

                return result;
            });

            return taskResult;
        }

        
        public async Task<ResultItem<TimeManagementEntriesRaw>> GetTimeManagementEntries(BaseConfig baseConfig, clsOntologyItem refItem, string idTimeManagementItem = null, TimeManagementEntryListFilter filterItem = null)
        {
            var taskResult = await Task.Run<ResultItem<TimeManagementEntriesRaw>>(() =>
            {
                var result = new ResultItem<TimeManagementEntriesRaw>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new TimeManagementEntriesRaw
                    {
                        TimeManagementEntries = new List<clsOntologyItem>(),
                        Attributes = new List<clsObjectAtt>(),
                        Relations = new List<clsObjectRel>()
                    }
                };

                var dbReaderTimeEntries = new OntologyModDBConnector(globals);

                List<clsObjectAtt> startItemsFiltered = null;
                if (filterItem != null)
                {
                    var filterRange = new DateTimeRangeFilter
                    {
                        Start = filterItem.RangeStart,
                        Ende = filterItem.RangeEnd
                    };

                    var searchAtt = new List<clsObjectAtt>
                    {
                        new clsObjectAtt
                        {
                            ID_Class = Config.LocalData.ClassAtt_Timemanagement_Start.ID_Class,
                            ID_AttributeType = Config.LocalData.ClassAtt_Timemanagement_Start.ID_AttributeType
                        }
                    };

                    var dbReaderAttFilter = new OntologyModDBConnector(globals);
                    result.ResultState = dbReaderAttFilter.GetDataObjectAtt(searchAtt, false, filter: filterRange);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the start-filter items!";
                        return result;
                    }

                    startItemsFiltered = dbReaderAttFilter.ObjAtts;
                }

                if (startItemsFiltered != null)
                {
                    var groupedObjectIds = startItemsFiltered.GroupBy(att => att.ID_Object).ToList();
                    var searchAttributesFiltered = groupedObjectIds.Select(attEntry => new clsObjectAtt
                    {
                        ID_AttributeType = Config.LocalData.ClassAtt_Timemanagement_Start.ID_AttributeType,
                        ID_Object = attEntry.Key
                    }).ToList();

                    searchAttributesFiltered.AddRange(groupedObjectIds.Select(attEntry => new clsObjectAtt
                    {
                        ID_AttributeType = Config.LocalData.ClassAtt_Timemanagement_Ende.ID_AttributeType,
                        ID_Object = attEntry.Key
                    }));

                    var dbReaderAttributes = new OntologyModDBConnector(globals);
                    if (searchAttributesFiltered.Any())
                    {
                        result.ResultState = dbReaderAttributes.GetDataObjectAtt(searchAttributesFiltered);
                        if (result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while getting the Attributes!";
                            return result;
                        }
                    }

                    result.Result.Attributes = dbReaderAttributes.ObjAtts;

                    var searchTimeManagementEntries = groupedObjectIds.Select(att => new clsOntologyItem
                    {
                        GUID = att.Key
                    }).ToList();

                    var dbReaderTimeManagement = new OntologyModDBConnector(globals);
                    if (searchTimeManagementEntries.Any())
                    {
                        result.ResultState = dbReaderTimeManagement.GetDataObjects(searchTimeManagementEntries);
                        if (result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            result.ResultState.Additional1 = "Error while getting the TimeManagement-Entries!";
                            return result;
                        }
                    }

                    result.Result.TimeManagementEntries = dbReaderTimeManagement.Objects1;

                    var searchTimeEntriesToRef = groupedObjectIds.Select(att => new clsObjectRel
                    {
                        ID_Object = att.Key,
                        ID_RelationType = Config.LocalData.ClassRel_Timemanagement_belonging_Resources.ID_RelationType
                    }).ToList();

                    var dbReaderReferences = new OntologyModDBConnector(globals);

                    if (searchTimeEntriesToRef.Any())
                    {
                        result.ResultState = dbReaderReferences.GetDataObjectRel(searchTimeEntriesToRef);

                        if (result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }
                    }

                    result.Result.TimeManagementEntriesToRef = dbReaderReferences.ObjectRels;
                }
                else
                {
                    if (refItem != null)
                    {
                        if (refItem.GUID_Parent == Config.LocalData.Class_Timemanagement.GUID)
                        {
                            result.Result.TimeManagementEntries.Add(refItem);
                        }
                        else
                        {
                            var searchTimeEntriesToRef = new List<clsObjectRel>
                            {
                                new clsObjectRel
                                {
                                    ID_Other = refItem?.GUID,
                                    ID_RelationType = Config.LocalData.ClassRel_Timemanagement_belonging_Resources.ID_RelationType,
                                    ID_Parent_Object = Config.LocalData.ClassRel_Timemanagement_belonging_Resources.ID_Class_Left
                                }
                            };

                            result.ResultState = dbReaderTimeEntries.GetDataObjectRel(searchTimeEntriesToRef);

                            if (result.ResultState.GUID == globals.LState_Error.GUID)
                            {
                                return result;
                            }

                            result.Result.TimeManagementEntriesToRef = dbReaderTimeEntries.ObjectRels;

                            result.Result.TimeManagementEntries = dbReaderTimeEntries.ObjectRels.Select(rel => new clsOntologyItem
                            {
                                GUID = rel.ID_Object,
                                Name = rel.Name_Object,
                                GUID_Parent = rel.ID_Parent_Object,
                                Type = globals.Type_Object
                            }).ToList();
                        }

                    }
                    else
                    {
                        var searchTimeEntries = new List<clsOntologyItem>();

                        if (!string.IsNullOrEmpty(idTimeManagementItem))
                        {
                            searchTimeEntries.Add(new clsOntologyItem
                            {
                                GUID = idTimeManagementItem
                            });
                        }
                        else
                        {
                            searchTimeEntries.Add(new clsOntologyItem
                            {
                                GUID_Parent = Config.LocalData.Class_Timemanagement.GUID
                            });
                        }

                        result.ResultState = dbReaderTimeEntries.GetDataObjects(searchTimeEntries);

                        if (result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            return result;
                        }

                        result.Result.TimeManagementEntries = dbReaderTimeEntries.Objects1;
                    }


                    var searchAttributes = result.Result.TimeManagementEntries.Select(timeEntry => new clsObjectAtt
                    {
                        ID_AttributeType = Config.LocalData.ClassAtt_Timemanagement_Start.ID_AttributeType,
                        ID_Object = timeEntry.GUID
                    }).ToList();

                    searchAttributes.AddRange(result.Result.TimeManagementEntries.Select(timeEntry => new clsObjectAtt
                    {
                        ID_AttributeType = Config.LocalData.ClassAtt_Timemanagement_Ende.ID_AttributeType,
                        ID_Object = timeEntry.GUID
                    }));

                    var dbReaderAttributes = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderAttributes.GetDataObjectAtt(searchAttributes);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    if (filterItem != null && !filterItem.All)
                    {
                        result.Result.Attributes = dbReaderAttributes.ObjAtts.Where(objAtt => objAtt.Val_Datetime >= filterItem.RangeStart && objAtt.Val_Date <= filterItem.RangeEnd).ToList();
                    }
                    else
                    {
                        result.Result.Attributes = dbReaderAttributes.ObjAtts;
                    }

                    if (refItem == null)
                    {
                        var searchTimeEntriesToRef = result.Result.TimeManagementEntries.Select(timeEntry => new clsObjectRel
                        {
                            ID_Object = timeEntry.GUID,
                            ID_RelationType = Config.LocalData.ClassRel_Timemanagement_belonging_Resources.ID_RelationType
                        }).ToList();

                        var dbReaderReferences = new OntologyModDBConnector(globals);

                        if (searchTimeEntriesToRef.Any())
                        {
                            result.ResultState = dbReaderReferences.GetDataObjectRel(searchTimeEntriesToRef);

                            if (result.ResultState.GUID == globals.LState_Error.GUID)
                            {
                                return result;
                            }
                        }

                        result.Result.TimeManagementEntriesToRef = dbReaderReferences.ObjectRels;
                    }
                }

                var searchRelations = result.Result.TimeManagementEntries.Select(timeEntry => new clsObjectRel
                {
                    ID_Object = timeEntry.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Timemanagement_is_in_State_Logstate.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Timemanagement_is_in_State_Logstate.ID_Class_Right
                }).ToList();

                searchRelations.AddRange(result.Result.TimeManagementEntries.Select(timeEntry => new clsObjectRel
                {
                    ID_Object = timeEntry.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Timemanagement_was_created_by_user.ID_RelationType,
                    ID_Other = baseConfig.IdUser
                }));

                searchRelations.AddRange(result.Result.TimeManagementEntries.Select(timeEntry => new clsObjectRel
                {
                    ID_Object = timeEntry.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_Timemanagement_belongs_to_Group.ID_RelationType,
                    ID_Other = baseConfig.IdGroup
                }));

                var dbReaderRelations = new OntologyModDBConnector(globals);

                if (searchRelations.Any())
                {
                    result.ResultState = dbReaderRelations.GetDataObjectRel(searchRelations);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                }
                
                result.Result.Relations = dbReaderRelations.ObjectRels;

                return result;
            });

            return taskResult;
        }

        
        public async Task<ResultItem<TimeManagementEntry>> SaveTimeManagementEntry(TimeManagementEntry entry, string propName)
        {
            var taskResult = await Task.Run<ResultItem<TimeManagementEntry>>(async () =>
            {
                var relationConfig = new clsRelationConfig(globals);
                var transaction = new clsTransaction(globals);
                var result = new ResultItem<TimeManagementEntry>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = entry
                };


                var oTimeManagementEntry = new clsOntologyItem
                {
                    GUID = string.IsNullOrEmpty( entry.IdTimeManagement) ? globals.NewGUID : entry.IdTimeManagement,
                    Name = entry.NameTimeManagement,
                    GUID_Parent = Config.LocalData.Class_Timemanagement.GUID,
                    Type = globals.Type_Object,
                    New_Item = string.IsNullOrEmpty(entry.IdTimeManagement)
                };

                
                if (oTimeManagementEntry.New_Item.Value || propName == nameof(TimeManagementEntry.NameTimeManagement))
                {
                    
                    result.ResultState = transaction.do_Transaction(oTimeManagementEntry);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }

                    entry.IdTimeManagement = oTimeManagementEntry.GUID;
                }


                if (propName == nameof(TimeManagementEntry.Start))
                {
                    var relStart = relationConfig.Rel_ObjectAttribute(oTimeManagementEntry, Config.LocalData.AttributeType_Start, entry.Start);

                    result.ResultState = transaction.do_Transaction(relStart, boolRemoveAll:true);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        transaction.rollback();
                        return result;
                    }

                    entry.IdAttributeStart = transaction.OItem_Last.OItemObjectAtt.ID_Attribute;
                }

                if (propName == nameof(TimeManagementEntry.Ende))
                {
                    var relEnde = relationConfig.Rel_ObjectAttribute(oTimeManagementEntry, Config.LocalData.AttributeType_Ende, entry.Ende);

                    result.ResultState = transaction.do_Transaction(relEnde, boolRemoveAll: true);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        transaction.rollback();
                        return result;
                    }

                    entry.IdAttributeEnde = transaction.OItem_Last.OItemObjectAtt.ID_Attribute;
                }

                if (propName == nameof(TimeManagementEntry.IdLogState))
                {
                    var logState = entry.IdLogState == Config.LocalData.Object_Work.GUID ? Config.LocalData.Object_Work
                                     : entry.IdLogState == Config.LocalData.Object_private.GUID ? Config.LocalData.Object_private
                                     : entry.IdLogState == Config.LocalData.Object_Urlaub.GUID ? Config.LocalData.Object_Urlaub
                                     : entry.IdLogState == Config.LocalData.Object_Krank.GUID ? Config.LocalData.Object_Krank : null;

                    if (logState == null)
                    {
                        var toDelete = new List<clsObjectRel>
                        {
                            new clsObjectRel
                            {
                                ID_Object = oTimeManagementEntry.GUID,
                                ID_Parent_Other = Config.LocalData.ClassRel_Timemanagement_is_in_State_Logstate.ID_Class_Right,
                                ID_RelationType = Config.LocalData.ClassRel_Timemanagement_is_in_State_Logstate.ID_RelationType
                            }
                        };

                        var dbDeletor = new OntologyModDBConnector(globals);

                        result.ResultState = dbDeletor.DelObjectRels(toDelete);
                    }
                    else
                    {
                        
                        var relEnde = relationConfig.Rel_ObjectRelation(oTimeManagementEntry, logState, Config.LocalData.RelationType_is_in_State);

                        result.ResultState = transaction.do_Transaction(relEnde, boolRemoveAll: true);
                        if (result.ResultState.GUID == globals.LState_Error.GUID)
                        {
                            transaction.rollback();
                            return result;
                        }

                        entry.NameLogState = logState.Name;
                    }
                    
                }

                if (propName == nameof(TimeManagementEntry.IdReference))
                {

                    var reference = await GetClassObject(entry.IdReference);
                    if (reference.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState = reference.ResultState;
                        return result;
                    }


                    var relEnde = relationConfig.Rel_ObjectRelation(oTimeManagementEntry, reference.Result.ObjectItem, Config.LocalData.RelationType_belonging_Resources);

                    result.ResultState = transaction.do_Transaction(relEnde, boolRemoveAll: true);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        transaction.rollback();
                        return result;
                    }

                    entry.NameLogState = reference.Result.ObjectItem.Name;
                    

                }

                if (propName == nameof(TimeManagementEntry.IdUser))
                {

                    var reference = await GetClassObject(entry.IdUser);
                    if (reference.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState = reference.ResultState;
                        return result;
                    }


                    var relUser = relationConfig.Rel_ObjectRelation(oTimeManagementEntry, reference.Result.ObjectItem, Config.LocalData.RelationType_was_created_by);

                    result.ResultState = transaction.do_Transaction(relUser, boolRemoveAll: true);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        transaction.rollback();
                        return result;
                    }

                    entry.NameLogState = reference.Result.ObjectItem.Name;


                }

                if (propName == nameof(TimeManagementEntry.IdGroup))
                {

                    var reference = await GetClassObject(entry.IdGroup);
                    if (reference.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState = reference.ResultState;
                        return result;
                    }


                    var relGroup = relationConfig.Rel_ObjectRelation(oTimeManagementEntry, reference.Result.ObjectItem, Config.LocalData.RelationType_belongs_to);

                    result.ResultState = transaction.do_Transaction(relGroup, boolRemoveAll: true);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        transaction.rollback();
                        return result;
                    }

                    entry.NameLogState = reference.Result.ObjectItem.Name;


                }

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> RemoveRefRel(TimeManagementEntry timeManagementItem)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var searchRelations = new List<clsObjectRel>
                { 
                    new clsObjectRel
                    {
                        ID_Object = timeManagementItem.IdTimeManagement,
                        ID_RelationType = Config.LocalData.RelationType_belonging_Resources.GUID
                    }
                };

                var elasticAgent = new OntologyModDBConnector(globals);
                result = elasticAgent.GetDataObjectRel(searchRelations);

                if (result.GUID == globals.LState_Error.GUID)
                {
                    result.Additional1 = "Error while getting the relations!";
                    return result;
                }

                var deleteRelations = elasticAgent.ObjectRels.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Object,
                    ID_RelationType = rel.ID_RelationType,
                    ID_Other = rel.ID_Other
                }).ToList();

                if (deleteRelations.Any())
                {
                    result = elasticAgent.DelObjectRels(deleteRelations);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Errro while deleting the relations!";
                        return result;
                    }
                }

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SaveRefRel(clsOntologyItem timeManagementItem, clsOntologyItem refItem)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var relationConfig = new clsRelationConfig(globals);
                var transaction = new clsTransaction(globals);

                var rel = relationConfig.Rel_ObjectRelation(timeManagementItem, refItem, Config.LocalData.RelationType_belonging_Resources);

                result = transaction.do_Transaction(rel);

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SaveUserGroup(clsOntologyItem timeManagementItem, clsOntologyItem userItem, clsOntologyItem groupItem)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var relationConfig = new clsRelationConfig(globals);
                var transaction = new clsTransaction(globals);

                var rel = relationConfig.Rel_ObjectRelation(timeManagementItem, userItem, Config.LocalData.RelationType_was_created_by);

                result = transaction.do_Transaction(rel);

                if (result.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                rel = relationConfig.Rel_ObjectRelation(timeManagementItem, groupItem, Config.LocalData.RelationType_belongs_to);

                result = transaction.do_Transaction(rel);

                if (result.GUID == globals.LState_Error.GUID)
                {
                    transaction.rollback();
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public ServiceAgentElastic(Globals globals) : base(globals)
        {
            this.globals = globals;
        }
    }
}
