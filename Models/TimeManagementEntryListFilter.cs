﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeManagementModule.Models
{
    public class TimeManagementEntryListFilter
    {
        public string RangeName { get; set; }
        public DateTime RangeStart { get; set; }
        public DateTime RangeEnd { get; set; }

        public bool All { get; set; }
    }
}
