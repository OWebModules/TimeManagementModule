﻿using OntologyClasses.BaseClasses;
using System.Collections.Generic;

namespace TimeManagementModule.Models
{
    public class TimeManagementEntriesRaw
    {
        public List<clsOntologyItem> TimeManagementEntries { get; set; }

        public List<clsObjectRel> TimeManagementEntriesToRef { get; set; } = new List<clsObjectRel>();
        public List<clsObjectAtt> Attributes { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectRel> Relations { get; set; } = new List<clsObjectRel>();
    }
}
