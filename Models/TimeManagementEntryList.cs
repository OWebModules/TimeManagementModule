﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeManagementModule.Models
{
    public class TimeManagementEntryList
    {
        public List<TimeManagementEntry> EntryList { get; set; }
        public DateTime LastEnd { get; set; }

        public TimeManagementEntryList()
        {
            LastEnd = DateTime.Now;
        }
    }
}
