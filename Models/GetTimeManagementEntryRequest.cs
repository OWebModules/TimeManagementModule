﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeManagementModule.Models
{
    public class GetTimeManagementEntryRequest
    {
        public string IdTimeManagementEntry { get; set; }
        public string IdReference { get; set; }

        public clsOntologyItem UserItem { get; private set; }
        public clsOntologyItem GroupItem { get; private set; }
        public IMessageOutput MessageOutput { get; set; }

        public GetTimeManagementEntryRequest(clsOntologyItem userItem, clsOntologyItem groupItem)
        {
            UserItem = userItem;
            GroupItem = groupItem;
        }
    }
}
