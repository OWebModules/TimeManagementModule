﻿using OntologyClasses.BaseClasses;

namespace TimeManagementModule.Models
{
    public class TimeManagementBaseConfigRaw
    {
        public clsOntologyItem User { get; set; }
        public clsOntologyItem Group { get; set; }
        public clsObjectRel UserGroupConfig { get; set; }
        public clsObjectAtt StandardHours { get; set; }
        public clsObjectAtt StandardDayCount { get; set; }

        public clsOntologyItem StopWatchConfig { get; set; }

        public clsOntologyItem ElasticSearchIndex { get; set; }

        public clsOntologyItem ElasticSearchType { get; set; }
    }
}
