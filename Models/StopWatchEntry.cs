﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeManagementModule.Models
{
    public class StopWatchEntry
    {
        public string Id { get; set; } = Guid.NewGuid().ToString();
        public string StopWatchId { get; set; }
        public int Mode { get; set; }
        public long OrderId { get; set; }
        public DateTime TimeStamp { get; set; }

        public Dictionary<string, object> GetDict()
        {
            var dict = new Dictionary<string, object>();
            dict.Add(nameof(StopWatchId), StopWatchId);
            dict.Add(nameof(Mode), Mode);
            dict.Add(nameof(TimeStamp), TimeStamp);

            return dict;
        }

        public StopWatchEntry(Dictionary<string,object> stopWatchEntryRaw)
        {
            if (stopWatchEntryRaw.ContainsKey(nameof(StopWatchId)))
            {
                StopWatchId = stopWatchEntryRaw[nameof(StopWatchId)].ToString();
            }
            if (stopWatchEntryRaw.ContainsKey(nameof(Mode)))
            {
                Mode = Convert.ToInt32((long) stopWatchEntryRaw[nameof(Mode)]);
            }
            if (stopWatchEntryRaw.ContainsKey(nameof(TimeStamp)))
            {
                TimeStamp = (DateTime)stopWatchEntryRaw[nameof(TimeStamp)];
            }
        }

        public StopWatchEntry(string stopWatchId, int mode, DateTime timeStamp)
        {
            StopWatchId = stopWatchId;
            Mode = mode;
            TimeStamp = timeStamp;
        }
    }
}
