﻿using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeManagementModule.Models
{
    public class StopWatchMeasureRequest
    {
        public string IdStopWatch { get; set; }
        public string IdUser { get; set; }
        public string IdGroup { get; set; }
        public IMessageOutput MessageOutput { get; set; }

        public StopWatchMeasureRequest(string idUser, string idGroup)
        {
            IdUser = idUser;
            IdGroup = idGroup;
        }
    }
}
