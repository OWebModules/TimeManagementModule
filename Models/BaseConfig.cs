﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeManagementModule.Services;

namespace TimeManagementModule.Models
{
    public class BaseConfig
    {
        private TimeManagementBaseConfigRaw baseConfigRaw;
        public TimeManagementBaseConfigRaw BaseConfigRaw
        {
            get { return baseConfigRaw; }
        }
        public string IdUser
        {
            get
            {
                return baseConfigRaw.User.GUID;
            }
        }
        public string NameUser
        {
            get
            {
                return baseConfigRaw.User.Name;
            }
        }
        public string IdGroup
        {
            get
            {
                return baseConfigRaw.Group.GUID;
            }
        }
        public string NameGroup
        {
            get
            {
                return baseConfigRaw.Group.Name;
            }
        }

        public string IdClassGroup
        {
            get
            {
                return baseConfigRaw.Group.GUID_Parent;
            }
        }

        public string IdAtributeStandardHours
        {
            get
            {
                return baseConfigRaw.StandardHours.ID_Attribute;
            }
        }
        public double StandardHours
        {
            get
            {
                return baseConfigRaw.StandardHours.Val_Double.Value;
            }
        }
        public string IdAttributeStandardDayCount
        {
            get
            {
                return baseConfigRaw.StandardDayCount.ID_Attribute;
            }
        }
        public double StandardDayCount
        {
            get
            {
                return baseConfigRaw.StandardDayCount.Val_Double.Value;
            }
        }

        public string IdStopWatchConfig
        {
            get
            {
                return baseConfigRaw.StopWatchConfig?.GUID;
            }
        }

        public string NameStopWatchConfig
        {
            get
            {
                return baseConfigRaw.StopWatchConfig?.Name;
            }
        }

        public string IdElasticSearchIndex
        {
            get
            {
                return baseConfigRaw.ElasticSearchIndex?.GUID;
            }
        }

        public string NameElasticSearchIndex
        {
            get
            {
                return baseConfigRaw.ElasticSearchIndex?.Name;
            }
        }

        public string IdElasticSearchType
        {
            get
            {
                return baseConfigRaw.ElasticSearchType?.GUID;
            }
        }

        public string NameElasticSearchType
        {
            get
            {
                return baseConfigRaw.ElasticSearchType?.Name;
            }
        }

        public List<TimeManagementEntryListFilter> FilterList
        {
            get; private set;
        } = new List<TimeManagementEntryListFilter>();

        public BaseConfig(TimeManagementBaseConfigRaw baseConfigRaw)
        {
            if (baseConfigRaw.User == null)
            {
                throw new Exception("Config Error!");
            }

            if (baseConfigRaw.Group == null)
            {
                throw new Exception("Config Error!");
            }

            if (baseConfigRaw.StandardHours == null)
            {
                throw new Exception("Config Error!");
            }

            if (baseConfigRaw.StandardDayCount == null)
            {
                throw new Exception("Config Error!");
            }
            this.baseConfigRaw = baseConfigRaw;

            var toDay = DateTime.Now.Date;
            FilterList.Add(new TimeManagementEntryListFilter
            {
                RangeName = "Today",
                RangeStart = toDay,
                RangeEnd = toDay.AddDays(1).Subtract(new TimeSpan(0, 0, 1))
            });
            FilterList.Add(new TimeManagementEntryListFilter
            {
                RangeName = "Yesterday",
                RangeStart = toDay.Date.AddDays(-1),
                RangeEnd = toDay.Date.Subtract(new TimeSpan(0, 0, 1))
            });

            
            var dayOfWeek = (int)DateTime.Now.DayOfWeek;
            dayOfWeek = (dayOfWeek == 0) ? dayOfWeek = 6 : dayOfWeek - 1;
            
            FilterList.Add(new TimeManagementEntryListFilter
            {
                RangeName = "This Week",
                RangeStart = toDay.Date.AddDays(-dayOfWeek),
                RangeEnd = toDay.Date.AddDays(1).Subtract(new TimeSpan(0, 0, 1))
            });

            FilterList.Add(new TimeManagementEntryListFilter
            {
                RangeName = "Last two Weeks",
                RangeStart = toDay.Date.AddDays(-(dayOfWeek + 7)),
                RangeEnd = toDay.Date.AddDays(1).Subtract(new TimeSpan(0, 0, 1))
            });

            var firstOfMonth = DateTime.Parse($"{1}.{toDay.Month}.{toDay.Year}");
            FilterList.Add(new TimeManagementEntryListFilter
            {
                RangeName = "This Month",
                RangeStart = firstOfMonth,
                RangeEnd = DateTime.Now.Date.AddDays(1).Subtract(new TimeSpan(0, 0, 1))
            });

            var firstOfLastMonth = DateTime.Parse($"{1}.{toDay.Month}.{toDay.Year}").AddMonths(-1);
            FilterList.Add(new TimeManagementEntryListFilter
            {
                RangeName = "Last Month",
                RangeStart = firstOfLastMonth,
                RangeEnd = DateTime.Now.Date.AddDays(1).Subtract(new TimeSpan(0, 0, 1))
            });

            FilterList.Add(new TimeManagementEntryListFilter
            {
                RangeName = "All",
                All = true
            });



        }
    }
}
