﻿using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeManagementModule.Models
{
    public enum StopWatchMode
    {
        Start = 0,
        Stop = 1,
        Reset = 2
    }
    public class StopWatchRequest
    {
        public StopWatchMode Mode { get; set; }
        public string IdStopWatch { get; set; }
        public string IdUser { get; set; }
        public string IdGroup { get; set; }
        public IMessageOutput MessageOutput {get; set;}

        public StopWatchRequest(StopWatchMode mode, string idUser, string idGroup)
        {
            Mode = mode;
            IdUser = idUser;
            IdGroup = idGroup;
        }
    }
}
