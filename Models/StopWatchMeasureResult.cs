﻿using OntologyAppDBConnector;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeManagementModule.Models
{
    public class StopWatchMeasureResult
    {
        public List<StopWatchEntry> StopWatchEntries { get; private set; }

        public ResultItem<TimeSpan> GetTime(Globals globals)
        {

            DateTime? start = null;
            DateTime? end = null;
            var result = new ResultItem<TimeSpan>
            {
                ResultState = globals.LState_Success.Clone()
            };
            foreach (var stopWatchEntry in StopWatchEntries.OrderBy(entry => entry.OrderId))
            {
                if (stopWatchEntry.Mode == (int)StopWatchMode.Start)
                {
                    start = stopWatchEntry.TimeStamp;
                }

                if (stopWatchEntry.Mode == (int)StopWatchMode.Reset)
                {
                    start = null;
                    end = null;
                }

                if (stopWatchEntry.Mode == (int)StopWatchMode.Stop)
                {
                    end = stopWatchEntry.TimeStamp;
                }
            }


            if (start == null && end  == null && !StopWatchEntries.Any())
            {
                result.ResultState = globals.LState_Nothing.Clone();
                result.Result = new TimeSpan(0);
                return result;
            }

            if (start == null)
            {
                result.ResultState = globals.LState_Error.Clone();
                result.ResultState.Additional1 = "No valid start!";
                return result;
            }

            if (end == null)
            {
                result.ResultState = globals.LState_Error.Clone();
                result.ResultState.Additional1 = "No valid end!";
                return result;
            }

            if (start.Value > end.Value)
            {
                result.ResultState = globals.LState_Error.Clone();
                result.ResultState.Additional1 = "Start > end!";
                return result;
            }

            result.Result = end.Value.Subtract(start.Value);

            return result;
        }

        public StopWatchMeasureResult(List<StopWatchEntry> stopWatchEntires)
        {
            StopWatchEntries = stopWatchEntires;
        }
    }
}
