﻿using OntoMsg_Module.Validation;
using OntoWebCore.Attributes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeManagementModule.Models
{
    [KendoGridConfig(
        groupbable = true,
        autoBind = false,
        resizable = true,
        selectable = SelectableType.cell,
        editable = EditableType.False,
        height = "500",
        width = "100%",
        scrollable = true)]
    //[KendoFilter(mode = "row")]
    [KendoPageable(buttonCount = 5, pageSize = 5, pageSizes = new int[] { 5, 10, 20, 30, 50, 100, 500, 1000 }, refresh = true)]
    [KendoStringFilterable(contains = "contains", eq = "equal", isempty = "Is empty", isnotnull = "Is not empty", neq = "Not equal", startsWith = "Wtarts With")]
    [KendoNumberFilterable(eq = "equal", isnotnull = "Is not empty", neq = "Not equal", gt = "Greater than", gte = "Greater than or equal", lt = "Lower than", lte = "Lower than or equal")]
    [KendoDateFilterable(eq = "equal", isnotnull = "Is not empty", neq = "Not equal", gt = "Greater than", gte = "Greater than or equal", lt = "Lower than", lte = "Lower than or equal")]
    [KendoBoolFilterable(eq = "equal", isnotnull = "Is not empty", neq = "Not equal", istrue = "W", isfalse = "F")]
    [KendoSortable(mode = SortType.multiple, allowUnsort = true, showIndexes = true)]
    public class TimeManagementEntry
    {
        [KendoColumn(hidden = true)]
        public string IdTimeManagement { get; set; }
        [KendoColumn(hidden = false, Order = 0, filterable = true, title = "Bezeichnung")]
        public string NameTimeManagement { get; set; }

        [KendoColumn(hidden = true)]
        public string IdLogState { get; set; }
        [KendoColumn(hidden = false, Order = 1, filterable = true, title = "Typ")]
        public string NameLogState { get; set; }

        [KendoColumn(hidden = true)]
        public string IdAttributeStart { get; set; }
        [KendoColumn(hidden = false, Order = 2, filterable = true, title = "Start", template = "#= Start != null ? kendo.toString(kendo.parseDate(Start), 'dd.MM.yyyy HH:mm:ss') : '' #", type = ColType.DateType)]
        public DateTime Start { get; set; }

        [KendoColumn(hidden = true)]
        public string IdAttributeEnde { get; set; }
        [KendoColumn(hidden = false, Order = 3, filterable = true, title = "Ende", template = "#= Ende != null ? kendo.toString(kendo.parseDate(Ende), 'dd.MM.yyyy HH:mm:ss') : '' #", type = ColType.DateType, width = "150")]
        public DateTime Ende { get; set; }

        [KendoColumn(hidden = false, Order = 4, filterable = true, title = "Wochentag (Start)", width = "150")]
        public string WeekDayStart { get; set; }
        [KendoColumn(hidden = false, Order = 5, filterable = true, title = "Wochentag (Ende)", width = "150")]
        public string WeekDayEnd { get; set; }

        [KendoColumn(hidden = false, Order = 6, filterable = true, title = "Stunden", type = ColType.NumberType, format = "{0:n2}", width = "150")]
        public double DurationHours { get; set; }
        [KendoColumn(hidden = false, Order = 7, filterable = true, title = "Minuten", type = ColType.NumberType, format = "{0:n2}", width = "150")]
        public double DurationMinutes { get; set; }
        [KendoColumn(hidden = false, Order = 8, filterable = true, title = "Stunden (Tag)", type = ColType.NumberType, format = "{0:n2}", width = "150")]
        public double DurationHoursDay { get; set; }
        [KendoColumn(hidden = false, Order = 9, filterable = true, title = "Minuten (Tag)", type = ColType.NumberType, format = "{0:n2}", width = "150")]
        public double DurationMinutesDay { get; set; }
        [KendoColumn(hidden = false, Order = 10, filterable = true, title = "Stunden (Woche)", type = ColType.NumberType, format = "{0:n2}", width = "150")]
        public double DurationHoursWeek { get; set; }
        [KendoColumn(hidden = false, Order = 11, filterable = true, title = "Minuten (Woche)", type = ColType.NumberType, format = "{0:n2}", width = "150")]
        public double DurationMinutesWeek { get; set; }

        [KendoColumn(hidden = false, Order = 12, filterable = true, title = "Stunden (Todo)", type = ColType.NumberType, format = "{0:n2}", width = "150")]
        public double ToDoHoursDay { get; set; }
        [KendoColumn(hidden = false, Order = 13, filterable = true, title = "Minuten (Todo)", type = ColType.NumberType, format = "{0:n2}", width = "150")]
        public double ToDoMinutesDay { get; set; }
        [KendoColumn(hidden = false, Order = 14, filterable = true, title = "Stunden (Todo Woche)", type = ColType.NumberType, format = "{0:n2}", width = "150")]
        public double ToDoHoursWeek { get; set; }
        [KendoColumn(hidden = false, Order = 15, filterable = true, title = "Minuten (Todo Woche)", type = ColType.NumberType, format = "{0:n2}", width = "150")]
        public double ToDoMinutesWeek { get; set; }

        [KendoColumn(hidden = false, Order = 16, filterable = true, title = "Geplantes Ende", template = "#= ToDoEnd != null ? kendo.toString(kendo.parseDate(ToDoEnd), 'dd.MM.yyyy hh:mm:ss') : '' #", type = ColType.DateType, width = "150")]
        public DateTime ToDoEnd { get; set; }

        [KendoColumn(hidden = true)]
        public string IdReference { get; set; }
        [KendoColumn(hidden = false, Order = 17, filterable = true, title = "Referenz", width = "150", template = "<button onClick='javascript:applyItem(this);'><i class='fa fa-check-square' aria-hidden='true'></i></button>#: NameReference #")]

        public string NameReference { get; set; }

        [KendoColumn(hidden = true)]
        public string IdParentReference { get; set; }

        [KendoColumn(hidden = false, Order = 18, filterable = true, title = "Referenz (Parent)", width = "150")]
        public string NameParentReference { get; set; }

        [KendoColumn(hidden = true)]
        public string IdGroup { get; set; }

        [KendoColumn(hidden = false, Order = 19, filterable = true, title = "Group", width = "150")]
        public string NameGroup { get; set; }

        [KendoColumn(hidden = true)]
        public string IdUser { get; set; }

        [KendoColumn(hidden = false, Order = 20, filterable = true, title = "User", width = "150")]
        public string NameUser { get; set; }

        [KendoColumn(hidden = true)]
        public long YearStart { get; set; }
        [KendoColumn(hidden = true)]
        public long MonthStart { get; set; }
        [KendoColumn(hidden = true)]
        public long DayStart { get; set; }
        [KendoColumn(hidden = true)]
        public long WeekStart { get; set; }
        [KendoColumn(hidden = true)]
        public long YearEnd { get; set; }
        [KendoColumn(hidden = true)]
        public long MonthEnd { get; set; }
        [KendoColumn(hidden = true)]
        public long DayEnd { get; set; }
        [KendoColumn(hidden = true)]
        public long WeekEnd { get; set; }

        [KendoColumn(hidden = true)]
        public long StartSeq { get; set; }
        [KendoColumn(hidden = true)]
        public long EndSeq { get; set; }

        private TimeManagementEntry undoItem;

        public TimeManagementEntry GetUndoItem()
        {
            return undoItem;
        }
        public void SetUndoItem()
        {
            undoItem = (TimeManagementEntry)this.MemberwiseClone();
        }

        public ValidationResult IsValid()
        {
            var result = new ValidationResult
            {
                IsValid = true
            };
            
            if (Start > Ende)
            {
                result.IsValid = false;
                result.AddMessage("Start is before Ende!");
            }

            if (string.IsNullOrEmpty(IdGroup))
            {
                result.IsValid = false;
                result.AddMessage("No group is set!");
            }

            if (string.IsNullOrEmpty(NameTimeManagement))
            {
                result.IsValid = false;
                result.AddMessage("No name is set!");
            }

            return result;
        }

        public bool IsDirty()
        {
            if (undoItem == null) return true;

            if (NameTimeManagement != undoItem.NameTimeManagement)
            {
                return true;
            }

            if (IdGroup != undoItem.IdGroup)
            {
                return true;
            }

            if (IdUser != undoItem.IdUser)
            {
                return true;
            }

            if (IdLogState != undoItem.IdLogState)
            {
                return true;
            }

            if (Start != undoItem.Start)
            {
                return true;
            }

            if (Ende != undoItem.Ende)
            {
                return true;
            }

            if (((string.IsNullOrEmpty(IdReference) ? null : IdReference) != (string.IsNullOrEmpty(undoItem.IdReference) ? null : undoItem.IdReference)))
            {
                return true;
            }

            return false;
        }

        public TimeManagementEntry() { }

        public TimeManagementEntry(string idTimeManagement,
            string nameTimeManagement,
            string idReference,
            string nameReference,
            string idParentReference,
            string nameParentReference,
            string idLogState,
            string nameLogState,
            string idAttributeStart,
            DateTime start,
            string idAttributeEnde,
            DateTime ende,
            string idGroup,
            string nameGroup,
            string idUser,
            string nameUser)
        {
            IdTimeManagement = idTimeManagement;
            NameTimeManagement = nameTimeManagement;
            IdReference = idReference;
            NameReference = nameReference;
            IdParentReference = idParentReference;
            NameParentReference = nameParentReference;
            IdLogState = idLogState;
            NameLogState = nameLogState;
            IdAttributeStart = idAttributeStart;
            Start = start;
            WeekDayStart = start.ToString("dddd");
            YearStart = start.Year;
            MonthStart = start.Month;
            DayStart = start.Day;
            WeekStart = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(start, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
            IdAttributeEnde = idAttributeEnde;
            Ende = ende;
            WeekDayEnd = ende.ToString("dddd");
            YearEnd = ende.Year;
            MonthEnd = ende.Month;
            DayEnd = ende.Day;
            WeekEnd = CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(ende, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
            DurationHours = ende.Subtract(start).TotalSeconds / 3600 * (idLogState == Config.LocalData.Object_private.GUID ? -1 : 1);
            DurationMinutes = ende.Subtract(start).TotalSeconds / 60 * (idLogState == Config.LocalData.Object_private.GUID ? -1 : 1);
            StartSeq = start.Year * 10000 + start.Month * 100 + start.Day;
            EndSeq = ende.Year * 10000 + ende.Month * 100 + ende.Day;
            IdGroup = idGroup;
            NameGroup = nameGroup;
            IdUser = idUser;
            NameUser = nameUser;
        }
    }

}
